#ifndef CRYPTO_H
#define CRYPTO_H



void decrypt(unsigned char *ptP, unsigned char *ptT, unsigned char *ptR)
{
    unsigned char *pP = NULL;
    unsigned char *pT = NULL;
    unsigned char *pR = NULL;
    for(pP=ptP,pT=ptT,pR=ptR;*pT!='\0';pT++,pP++,pR++)
    {
        if (*pP=='\0')pP=ptP;
        if (*pT=='\0') *pR= *pP;
        else *pR= *pT + *pP;
    }
    pR++;
    *pR='\0';

}

void encrypt(unsigned char *ptP, unsigned char *ptT, unsigned char *ptR)
{
    unsigned char *pP = NULL;
    unsigned char *pT = NULL;
    unsigned char *pR = NULL;

    for (pP=ptP,pT=ptT,pR=ptR;*pT!='\0';pT++,pP++,pR++)
    {
        if (*pP=='\0')pP=ptP;
        *pR= *pT - *pP;
    }

}

#endif // CRYPTO_H
